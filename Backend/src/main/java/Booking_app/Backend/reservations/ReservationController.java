package Booking_app.Backend.reservations;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("api-booking-app")
public class ReservationController {

  @Autowired
	ReservationRepository repository;

 
  @GetMapping("/reservations")
   public List<Reservations> getAllReservations() {
    return repository.findAll();
  }


  @PostMapping("/newReservation")
  Reservations newReservation(@RequestBody Reservations newReservation) {
    return repository.save(newReservation);
  }

}
