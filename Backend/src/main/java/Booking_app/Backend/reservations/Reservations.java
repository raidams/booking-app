package Booking_app.Backend.reservations;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "reservations")
public class Reservations {
  @GeneratedValue(strategy=GenerationType.AUTO)
    @Id
    private String id;

    @Column(name="name")
    private String name;

    @Column(name="email")
    private String email;

    @Column(name="number")
    private String number;

    @Column(name="service")
    private String service;

    @Column(name="date")
    private String date;

    @Column(name="timestamp")
    private String timestamp;
  

    protected Reservations() {}
  
    public Reservations(String name, String email, String number, String service, String date, String timestamp) {
      this.name = name;
      this.email = email;
      this.number = number;
      this.service = service;
      this.date = date;
      this.timestamp = timestamp;

    }
  
   
    @Override
    public String toString() {
      return "Reservations [id=" + id + ", name=" + name + ", email=" + email + ", number=" + number + ", service="
          + service + ", date=" + date + ", timestamp=" + timestamp + "]";
    }

    
    public String getId() {
      return id;
    }
  
    public String getName() {
      return name;
    }
  
    public String getEmail() {
      return email;
    }

    public void setId(String id) {
      this.id = id;
    }

    public void setName(String name) {
      this.name = name;
    }

    public void setEmail(String email) {
      this.email = email;
    }

    public String getNumber() {
      return number;
    }

    public void setNumber(String number) {
      this.number = number;
    }

    public String getService() {
      return service;
    }

    public void setService(String service) {
      this.service = service;
    }

    public String getDate() {
      return date;
    }

    public void setDate(String date) {
      this.date = date;
    }

    public String getTimestamp() {
      return timestamp;
    }

    public void setTimestamp(String timestamp) {
      this.timestamp = timestamp;
    }
    
}
