package Booking_app.Backend.reservations;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface ReservationRepository extends JpaRepository<Reservations, String> {

}

//List<Reservations> findByLastName(String name);