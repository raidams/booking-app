package Booking_app.Backend.priceList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("api-booking-app")
public class PriceController {

    @Autowired
    PriceListRepository repository;

 
    @GetMapping("/preise")
    public List<PriceList> getPriceList() {
    return repository.findAll();
  }
    
}
