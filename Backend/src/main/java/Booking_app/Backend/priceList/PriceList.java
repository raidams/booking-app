package Booking_app.Backend.priceList;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

@Entity
@Table(name = "priceList")
public class PriceList {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    @SequenceGenerator(name = "seq", sequenceName = "id_seq", allocationSize = 1)
    private String id;

    @Column(name="service")
    private String service;

    @Column(name="price")
    private int price;
    

    protected PriceList() {}

    public PriceList(String service, int price) {
        this.service = service;
        this.price = price;

    }


    @Override
    public String toString() {
        return "PriceList [service=" + service + ", price=" + price + "]";
    }


    public void setService(String service) {
        this.service = service;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getService() {
        return service;
    }

    public int getPrice() {
        return price;
    }
}
