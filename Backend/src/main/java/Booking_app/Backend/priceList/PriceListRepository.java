package Booking_app.Backend.priceList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
interface PriceListRepository extends JpaRepository<PriceList, String>{
    
}
