import http from "../http-common.js";

class DataServices {
  getAll() {
    return http.get("/reservations");
  }

  getPriceList() {
    return http.get("/preise");
  }

  /*findByTitle(title) {
    return http.get(`/items?title=${title}`);
  }*/
}

export default new DataServices();
