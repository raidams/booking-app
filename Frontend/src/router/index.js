import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import AboutView from "../views/AboutView.vue";
import PreisView from "../views/PreisView.vue";
import GallerieView from "../views/GallerieView.vue";
import TerminView from "../views/TerminView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "/",
      component: HomeView,
    },
    {
      path: "/home",
      name: "home",
      component: HomeView,
    },
    {
      path: "/about",
      name: "about",
      component: AboutView,
    },
    {
      path: "/preise",
      name: "preise",
      component: PreisView,
    },
    {
      path: "/gallerie",
      name: "gallerie",
      component: GallerieView,
    },
    {
      path: "/termin",
      name: "termin",
      component: TerminView,
    },
  ],
});

export default router;
