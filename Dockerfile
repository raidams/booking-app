FROM node:20.11.0-alpine

RUN apk add --no-cache openjdk21 maven \
    && apk update \
    && apk upgrade 

RUN npm install -g @vue/cli

